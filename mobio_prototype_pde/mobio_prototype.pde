/*-------------------------------------------- ECOUTER CHUCHOTER LES MURS
    mobio-prototype, sept. 2010
    
    Copyright (C) <2010>  <Pierre Commenge, Thomas Bernardi, Echelle Inconnue>
    TinyGPS & NewSoftSerial libraries by Mikal Hart
    
    T. Bernardi - http://madosedesoma.free.fr
    P. Commenge - http://emoc.org
    Echelle Inconnnue - http://echelleinconnue.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
    aout/septembre 2010 / projet Smala Marseille
    
    Des zones circulaires sont définies par leurs coordonnées (zoneLat, zoneLong)
    et leur "portee" maximum (zoneSize).
    Les coordonnées réelles issués du GPS sont comparées à ces zones prédéfinies
    A chaque boucle on cherche si on est dans une zone, selon les reponses un son
    est mis en lecture
    
     
    - quand 2 zones se superposent ??
      -> c'est la derniere dans le parcours de la boucle qui est considérée comme valide
    
    
    clignotement des leds
    led rouge : on / off
    led verte : clignote X fois selon la zone dans laquelle on se trouve
    
    
*/

#include <NewSoftSerial.h>
#include <TinyGPS.h>
#include <math.h>               // include the Math Library



TinyGPS gps;
NewSoftSerial nss(9, 8); // receive, transmit
NewSoftSerial mp3(2, 3); // receive, transmit

void gpsdump(TinyGPS &gps);
bool feedgps();
void printFloat(double f, int digits = 2);
float fflat, fflon;

int ledPinOnOff =  6; // rouge
int ledPinZone =  4;  // verte
int busyPin = 5;
int busyVal = 1000; // valeur de lecture par défaut du pin busy (0 = actif)

long boucle = 0; // nombre de boucles effectuees

float dist;
int zone = -1;               // zone en cours
int zoneLast = -1;           // derniere zone vue

int zones = 14;              // combien de zones ?
long zoneLat[14]  = { 4336997, 4337008, 4336876, 4336826, 4336857, 4336873, 4336941, 4336826, 4336806, 4336895, 4336944, 4336955, 4337011, 4336760};  // coord. x des centres de zone
long zoneLon[14]  = {  535305,  535070,  534999,  535341,  535175,  535266,  535241,  535088,  534994,  535090,  535023,  535142,  535209,  535062};  // coord. y des centres de zone
long zoneSize[14] = {      30,      33,      31,      36,      31,      30,      33,      31,      31,      30,      31,      33,      32,      29}; // rayon max pour chaque zone




void setup()
{
    Serial.begin(115200);
    nss.begin(4800);
    mp3.begin(4800);
      
    Serial.print("Testing TinyGPS library v. "); Serial.println(TinyGPS::library_version());
    Serial.println("by Mikal Hart");
    Serial.println();
    Serial.print("Sizeof(gpsobject) = "); Serial.println(sizeof(TinyGPS));
    Serial.println();
    
    pinMode(ledPinOnOff, OUTPUT);
    pinMode(ledPinZone, OUTPUT);
    pinMode(busyPin, INPUT);
    
    
    for (int i=0; i <= 25; i++){ // monter le son du mp3
      mp3.print(232, BYTE);
    }
    mp3.print(241, HEX); // change to root folder
    
}
  
void loop()  {
  
  boucle++;
  Serial.print("boucle : ");
  Serial.print(boucle);
  Serial.print(" / millis : ");
  Serial.println(millis());
  
  /*
  
  if (millis() < 120000) { // pendant les 2 premieres minutes, synchro, rien ne se passe
    
    digitalWrite(ledPinZone, HIGH); 
    digitalWrite(ledPinOnOff, LOW); 
    delay(1000);                  
    digitalWrite(ledPinZone, LOW);
    digitalWrite(ledPinOnOff, HIGH);  
    delay(1000);  
    
  } else {*/
        
    bool newdata = false;
    unsigned long start = millis();
    
    /* mise à jour des leds *************************************** */
    
    if (zone > -1) {
        int cligne = zone;
        for (int j=0; j < cligne; j++){
          digitalWrite(ledPinZone, HIGH); 
          delay(50);                  
          digitalWrite(ledPinZone, LOW); 
          delay(100);                  
        }
        //delay(1000);
    } else {
          digitalWrite(ledPinZone, HIGH); 
          delay(500);                  
          digitalWrite(ledPinZone, LOW); 
          //delay(1000);  
    }
    
    digitalWrite(ledPinOnOff, HIGH);   // set the LED on
    
    Serial.print("fin de mise a jour des leds a ");
    Serial.println(millis());
    /* ************************************************************ */
  
  
    // Every X seconds we print an update
    while (millis() - start < 3000)
    {
      if (feedgps())
        newdata = true;
    }
    
    if (newdata)
    {
      Serial.println("Acquired Data");
      Serial.println("-------------");
      gpsdump(gps);
      Serial.println("-------------");
      Serial.println();
    }
    
    Serial.print("latitude (fflat): "); Serial.println(fflat);
    Serial.print("longitude (fflon): "); Serial.println(fflon);
    
    
    
    zoneLast = zone;
    zone = -1;
    
    for (int i=0; i < zones; i++){ // parcourir zone par zone
      Serial.print("distance zone "); Serial.print(i+1);
      Serial.print(" : "); Serial.print("fflat: ");
      Serial.print(fflat); Serial.print(", fflon: ");
      Serial.print(fflon); Serial.print(", zoneLat[i]: ");
      Serial.print(zoneLat[i]); Serial.print(", zoneLon[i]: ");
      Serial.print(zoneLon[i]); Serial.print(", dist: ");
  
      dist = getDist(fflat, fflon, zoneLat[i], zoneLon[i]);
      if (dist < 0) dist = -dist;
      Serial.println(dist);
      
      if (dist <= zoneSize[i]) { // PAS BON
        zone = i+1;
      }
    }
    
    /* lecture en cours ? ***************************************** */
    
    boolean sonJoue = false;
    busyVal = 1;
    busyVal = analogRead(busyPin);
    Serial.print("valeur du pin busy : ");
    Serial.println(busyVal);
    if (busyVal < 100) sonJoue = true;
    
    if (sonJoue == false) {
      
      if (zone == -1) {
        
        mp3.print(239, BYTE); // stopper le son
        Serial.println("plus dans la portee d'aucune zone");
        
      } else {
        
        if (zone == 1) { mp3.print(1, BYTE); }
        if (zone == 2) { mp3.print(11, BYTE); }
        if (zone == 3) { mp3.print(14, BYTE); }
        if (zone == 4) { mp3.print(5, BYTE); }
        if (zone == 5) { mp3.print(6, BYTE); }
        if (zone == 6) { mp3.print(4, BYTE); }
        if (zone == 7) { mp3.print(12, BYTE); }
        if (zone == 8) { mp3.print(9, BYTE); }
        if (zone == 9) { mp3.print(13, BYTE); }
        if (zone == 10) { mp3.print(8, BYTE); }
        if (zone == 11) { mp3.print(2, BYTE); }
        if (zone == 12) { mp3.print(7, BYTE); }
        if (zone == 13) { mp3.print(3, BYTE); }
        if (zone == 14) { mp3.print(10, BYTE); } 
      }
    }
 
    Serial.print("zone : ");
    Serial.println(zone);
    Serial.println("---------------------");
  //}
}

float getDist(long fflat, long fflon, long latz, long lonz) {
    long a = fflat - latz;
    long b = fflon - lonz;
    long d = sqrt( a*a + b*b );
    
    return d;
}

void printFloat(double number, int digits)
{
  // Handle negative numbers
  if (number < 0.0)
  {
     Serial.print('-');
     number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (uint8_t i=0; i<digits; ++i)
    rounding /= 10.0;
  
  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;
  double remainder = number - (double)int_part;
  Serial.print(int_part);

  // Print the decimal point, but only if there are digits beyond
  if (digits > 0)
    Serial.print("."); 

  // Extract digits from the remainder one at a time
  while (digits-- > 0)
  {
    remainder *= 10.0;
    int toPrint = int(remainder);
    Serial.print(toPrint);
    remainder -= toPrint; 
  } 
}

void gpsdump(TinyGPS &gps)
{
  long lat, lon;
  float flat, flon;
  unsigned long age, date, time, chars;
  int year;
  byte month, day, hour, minute, second, hundredths;
  unsigned short sentences, failed;

  gps.get_position(&lat, &lon, &age);
  Serial.print("Lat/Long(10^-5 deg): "); Serial.print(lat); Serial.print(", "); Serial.print(lon); 
  Serial.print(" Fix age: "); Serial.print(age); Serial.println("ms.");
  
  feedgps(); // If we don't feed the gps during this long routine, we may drop characters and get checksum errors

  gps.f_get_position(&flat, &flon, &age);
  fflat = lat;
  fflon = lon;
  Serial.print("Lat/Long(float): "); printFloat(flat, 5); Serial.print(", "); printFloat(flon, 5);
  Serial.print(" Fix age: "); Serial.print(age); Serial.println("ms.");

  feedgps();

  gps.get_datetime(&date, &time, &age);
  Serial.print("Date(ddmmyy): "); Serial.print(date); Serial.print(" Time(hhmmsscc): "); Serial.print(time);
  Serial.print(" Fix age: "); Serial.print(age); Serial.println("ms.");

  feedgps();

  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  Serial.print("Date: "); Serial.print(static_cast<int>(month)); Serial.print("/"); Serial.print(static_cast<int>(day)); Serial.print("/"); Serial.print(year);
  Serial.print("  Time: "); Serial.print(static_cast<int>(hour)); Serial.print(":"); Serial.print(static_cast<int>(minute)); Serial.print(":"); Serial.print(static_cast<int>(second)); Serial.print("."); Serial.print(static_cast<int>(hundredths));
  Serial.print("  Fix age: ");  Serial.print(age); Serial.println("ms.");
  
  feedgps();

  Serial.print("Alt(cm): "); Serial.print(gps.altitude()); Serial.print(" Course(10^-2 deg): "); Serial.print(gps.course()); Serial.print(" Speed(10^-2 knots): "); Serial.println(gps.speed());
  Serial.print("Alt(float): "); printFloat(gps.f_altitude()); Serial.print(" Course(float): "); printFloat(gps.f_course()); Serial.println();
  Serial.print("Speed(knots): "); printFloat(gps.f_speed_knots()); Serial.print(" (mph): ");  printFloat(gps.f_speed_mph());
  Serial.print(" (mps): "); printFloat(gps.f_speed_mps()); Serial.print(" (kmph): "); printFloat(gps.f_speed_kmph()); Serial.println();

  feedgps();

  gps.stats(&chars, &sentences, &failed);
  Serial.print("Stats: characters: "); Serial.print(chars); Serial.print(" sentences: "); Serial.print(sentences); Serial.print(" failed checksum: "); Serial.println(failed);
}
  
bool feedgps()
{
  while (nss.available())
  {
    if (gps.encode(nss.read()))
      return true;
  }
  return false;
}
